# Ghost CMS

A deployment of Ghost using a custom frontend

## Usage
- Make a copy of the example env file: `cp example.env .env`
- Replace the values in `.env`